public with sharing class MatchHelper {
    @AuraEnabled(cacheable=false)
    public static List<Match__c> getMatchList() {
        return [SELECT Id, Name, HomeTeam__c , AwayTeam__c ,
                HomeTeamScore__c , AwayTeamScore__c 
                FROM Match__c];
    }
}