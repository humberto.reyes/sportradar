import { LightningElement ,api, wire, track} from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import getMatchList from '@salesforce/apex/MatchHelper.getMatchList';
import HOME_TEAM_FIELD from '@salesforce/schema/Match__c.HomeTeam__c';
import AWAY_TEAM_FIELD from '@salesforce/schema/Match__c.AwayTeam__c';
import MATCH_ID_FIELD from '@salesforce/schema/Match__c.Id';
import MATCH_OBJECT from '@salesforce/schema/Match__c';

export default class ScoreBoard extends LightningElement {

    @api recordId;
    objectApiName = MATCH_OBJECT;
    fields = [HOME_TEAM_FIELD, AWAY_TEAM_FIELD];    
    @track error;
    @track matchList ;
    @track isModalOpen = false;

    connectedCallback(){
        this.getMatchs();
   }      
   handleSubmit(event) {
        
    event.preventDefault(); // stop the form from submitting
    const fields = event.detail.fields;
    fields.MATCH_ID_FIELD = this.recordId;
    this.template.querySelector('lightning-record-form').submit(fields);
}

handleSuccess(event) {
    const evt = new ShowToastEvent({
        title: 'Match Started',
        message: 'Record ID: ' + event.detail.id,
        variant: 'success',

    });
    
    this.dispatchEvent(evt);
}

   getMatchs() {
    getMatchList({ })
    .then(result => {
        this.matchList = result;
        this.error = undefined;
    })
    .catch(error => {
        this.error = error;
        this.matchList = undefined;
    })
    }

    openModal() {
        // to open modal set isModalOpen tarck value as true
        this.isModalOpen = true;
    }
    closeModal() {
        // to close modal set isModalOpen tarck value as false
        this.isModalOpen = false;
    }
    submitDetails() {
        // to close modal set isModalOpen tarck value as false
        //Add your code to call apex method or do some processing
        this.isModalOpen = false;
    }    
}